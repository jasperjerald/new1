/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spider.learn.jdbc.JPA;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class JPAMain {

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Test");
        EntityManager em = emf.createEntityManager();
        List<Author> author = em.createQuery("Select c from Author c", Author.class).getResultList();
        for (Author i : author) {
            System.out.println(i.getId() + "," + i.getName() + "," + i.getEmail());
        }
        em.close();
        emf.close();
    }
}
